/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.midterm64_02;

/**
 *
 * @author ACER
 */
public class TestInsect {
    public static void main(String[] args) {
        Insect insect = new Insect("Insect","Black",0,0); //(ชื่อแมลง,สีแมลง,ขาแมลง,ปีกแมลง)
        insect.walk();
        insect.fly();
        System.out.println("-------------");
        Beetle beetle = new Beetle("Legoshi","Black and Brown"); //(ชื่อด้วง,สีด้วง,ขาด้วง,ปีกด้วง)
        beetle.walk();
        beetle.fly();
        System.out.println("-------------");
        Bee bee = new Bee("BeeBee","Yellow and Black"); //(ชื่อผึ้ง,สีด้วง,ขาด้วง,ปีกด้วง)
        bee.walk();
        bee.fly();
        System.out.println("-------------");
        Dragonfly Joe = new Dragonfly("Joe","Blue and Green"); //(ชื่อแมลงปอ,สีด้วง,ขาด้วง,ปีกด้วง)
        Joe.walk();
        Joe.fly();
    }
}
