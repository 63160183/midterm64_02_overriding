/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.midterm64_02;

/**
 *
 * @author ACER
 */
public class Insect {

    protected String name;
    protected int numLegs = 0;
    protected int numWing = 0;
    protected String colorBody;

    public Insect(String name, String colorBody,int numLegs,int numWing) {
        System.out.println("Insect created"); //ปริ้น classInsect
        this.name = name; 
        this.colorBody = colorBody;
        this.numLegs = numLegs;
        this.numWing = numWing;
         System.out.println("Name: "+ this.name+" Legs: "+this.numLegs+" color: "+this.colorBody
                +" Wings: "+this.numWing);
    }
    public void walk(){ //ให้แมลงเดิน
        System.out.println("Insect is walk");
    }
    public void fly(){ //ให้แมลงบิน
        System.out.println("Insect is Fly!!!");
        System.out.println("Name: "+ this.name+" Legs: "+this.numLegs+" color: "+this.colorBody
                +" Wings: "+this.numWing);
    }

    public String getName() {
        return name;
    }

    public int getNumLegs() {
        return numLegs;
    }

    public int getNumWing() {
        return numWing;
    }

    public String getColorBody() {
        return colorBody;
    }
    
}
