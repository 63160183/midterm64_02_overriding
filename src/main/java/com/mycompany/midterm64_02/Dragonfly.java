/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.midterm64_02;

/**
 *
 * @author ACER
 */
public class Dragonfly extends Insect{ //สร้าง classDragonfly สืบทอดมาจาก Insect
    public Dragonfly(String name, String colorBody){
        super(name,colorBody,6,4); //super(ชื่อ,สีตัว,จำนวนขาของแมลงปอ,จำนวนปีกของแมลงปอ);
        this.name = name;
        this.colorBody = colorBody;
        System.out.println("Dragonfly is created");
        System.out.println(name+" is created");
    }
    @Override
    public void walk(){ //method ให้แมลงปอเดิน
        System.out.println(name+" walk with legs: "+numLegs+" legs");
    }
    @Override
    public void fly(){ //method ให้แมลงปอบิน
        System.out.println(name+" fly with: "+numWing+" wings");
    }
}
