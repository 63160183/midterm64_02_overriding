/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.midterm64_02;

/**
 *
 * @author ACER
 */
public class Bee extends Insect{
    public Bee(String name, String colorBody){
        super(name,colorBody,3,2); //super(ชื่อ,สีตามตัว,จำนวนขาผึ้ง,จำนวนปีกของผึ้ง);
        this.name = name;
        this.colorBody = colorBody;
        System.out.println("Bee Created.");
        System.out.println(name+" is created");
    }
    @Override
    public void walk(){ // method ให้ผึ้งเดิน
        System.out.println(name+" walk with legs: "+numLegs+" legs");
    }
    @Override
     public void fly(){ //method ให้ผึ้งบิน
        System.out.println(name+" fly with: "+numWing+" wings");
    }
}
