/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.midterm64_02;

/**
 *
 * @author ACER
 */
public class Beetle extends Insect{ //สร้าง classBeetle ให้สืบทอดไปยัง Insect
    public Beetle(String name, String colorBody){
        super(name,colorBody,6,2); //super(ชื่อ,สีเปลือกตัว,จำนวนขาด้วง,จำนวนปีกด้วง);
        this.name = name;
        this.colorBody = colorBody;
        System.out.println("Beetle Created.");
        System.out.println(name+" is created");
        
    }
    @Override
    public void walk(){ //method ให้ด้วงเดิน Override สืบทอดมาจาก Insect
        System.out.println(name+" walk with legs: "+numLegs+" legs");
    }
    @Override
    public void fly(){ //mthod ให้ด้วงบิน Override สืบทอดมาจาก Insect
         System.out.println(name+" fly with: "+numWing+" wings");
    }
}
